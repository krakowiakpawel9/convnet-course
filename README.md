# convnet-course

Repozytorium jest częścią kursu [Deep Learning w języku Python - Konwolucyjne Sieci Neuronowe](https://www.udemy.com/deep-learning-w-jezyku-python/) dostępnego na platformie Udemy.com

Stephen Hawking powiedział kiedyś: "whatever you want to uncover the secrets of the universe, or you just want to pursue a career in the 21st century, basic computer programming is an essential skill to learn".  Te słowa jakże znamiennie wybrzmiewają w dzisiejszych czasach. Nie ulega wątpliwości, że przyszłość zależeć będzie w dużej mierze od postępu technologicznego i ludzi, którzy będą w stanie ten postęp kreować. Jak pokazują dane, utrzymujące się ogromne zapotrzebowanie na specjalistów w sektorze technologii powoduje, że zarobki w branży także są bardzo satysfakcjonujące. 

Sztuczna Inteligencja (Artificial Intelligence) rośnie w tempie wykładniczym. Od prostych modeli klasyfikujących pocztę mailową, wybierającą najbardziej optymalną trasę dojazdu, rozpoznającą nas w czasie rzeczywistym (wideoweryfikacja) po auta a nawet samoloty autonomiczne. A przed nami przecież tyle nieodkrytych obszarów w których można zastosować AI.

### Do czego służy biblioteka Keras?

Keras to biblioteka open source do tworzenia sieci neuronowych, która jako backend wykorzystuje Tensorflow, CNTK, czy Theano. Jest doskonałym narzędziem do prototypowania i eksperymentowania oszczędzając nam wiele czasu przy pisaniu kodu. Stanowi także niską barierę wejścia dla osób, które dopiero zaczynają swoją karierę w uczeniu maszynowym.

### Wzrost popularności języka Python

Język Python świetnie nadaje się do przetwarzania, przygotowania, analizy i modelowania danych. Jest prosty do nauki i nie powinien sprawiać problemów osobie, która dopiero zaczyna uczyć się programowania. Na przestrzeni ostatnich lat i rosnącej popularności sztucznej inteligencji Python wyrósł na gwiazdę w tym sektorze. Powstało sporo potężnych bibliotek do machine learningu, czy deep learningu. Przykładem może być stworzona przez Google biblioteka Tensorflow.

### Wszechstronność Zastosowań

Języka Python możemy używać na bardzo wielu poziomach. Czy to u siebie w domu, w biurze, czy uruchamiając skrypty w chmurze. W połączeniu z mnogością zastosowań i doskonałym zestawem narzędzi takich jak Spyder, Jupyter Notebook, Zeppelin, IPython i innymi bibliotekami środowisko do analizy danych w Pythonie wyróżnia się wydajnością, produktywnością i doskonałą elastycznością.
